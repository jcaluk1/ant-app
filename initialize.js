const { sequelize, GroceryItem } = require("./models");
const initialize = async () => {
    const groceries = [
        {
            name: "Apple",
            quantityUnit: "kg",
            image: "svg/apple.svg"
        },
        {
            name: "Bread",
            image: "svg/bread.svg"

        },
        {
            name: "Chicken legs",
            quantityUnit: "kg",
            image: "svg/chicken-leg.svg"

        },
        {
            name: "Chop",
            quantityUnit: "kg",
            image: "svg/chop.svg"
        },
        {
            name: "Eggs",
            quantityUnit: "dozen",
            image: "svg/eggs.svg"

        },
        {
            name: "Leek",
            quantityUnit: "kg",
            image: "svg/leek.svg"
        },
        {
            name: "Milk",
            quantityUnit: "l",
            image: "svg/milk.svg"
        },
        {
            name: "Orange",
            quantityUnit: "kg",
            image: "svg/orange.svg"
        },
        {
            name: "Fish",
            quantityUnit: "kg",
            image: "svg/fish.svg"
        },
        {
            name: "Flour",
            quantityUnit: "kg",
            image: "svg/flour.svg"
        },
        {
            name: "Honey",
            quantityUnit: "kg",
            image: "svg/honey.svg"
        },
        {
            name: "Juice",
            quantityUnit: "l",
            image: "svg/juice-box.svg"
        },
        {
            name: "Potato",
            quantityUnit: "kg",
            image: "svg/potato.svg"
        },
        {
            name: "Spinach",
            quantityUnit: "kg",
            image: "svg/spinach.svg"
        },
        {
            name: "Tomato",
            quantityUnit: "kg",
            image: "svg/tomato.svg"
        },
        {
            name: "Yogurt",
            quantityUnit: "l",
            image: "svg/yogurt.svg"
        }
    ];
    await sequelize.sync({ alter: true });
    const count = await GroceryItem.count();
    if (!count) {
        await GroceryItem.bulkCreate(groceries);
    }
};
initialize();