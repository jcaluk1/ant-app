const { DataTypes } = require("sequelize");
module.exports = (sequelize) => sequelize.define("grocerylist",
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull:false
        },
        description: {
            type: DataTypes.STRING
        },
        isCompleted: {
            type: DataTypes.BOOLEAN,
            allowNull:false,
            defaultValue: false
        }
    });