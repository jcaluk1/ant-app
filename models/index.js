const {Sequelize, Op} = require("sequelize");

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./db.sqlite",
    logging: function(){},
    define: {
        freezeTableName: true
    }
});

const GroceryList = require("./GroceryList")(sequelize);
const GroceryItem = require("./GroceryItem")(sequelize);
const ItemList = require("./ItemList")(sequelize);

GroceryItem.belongsToMany(GroceryList, { through: ItemList});
GroceryList.belongsToMany(GroceryItem, { through: ItemList});

const db = {sequelize, GroceryList, GroceryItem, ItemList, Op};
module.exports = db;