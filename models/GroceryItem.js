const { DataTypes } = require("sequelize");
module.exports = (sequelize) => sequelize.define("groceryitem",
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull:false
        },
        quantityUnit: {
            type: DataTypes.STRING,
        },
        image: {
            type: DataTypes.STRING,
        }
    });