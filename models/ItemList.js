const { DataTypes } = require("sequelize");
module.exports = (sequelize) => sequelize.define("itemlist",
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    });