const asyncHandler = require("../middleware/asynchandler");
const {createListService, getAllListsService, getOneListService, deleteListService, updateListService} = require("../services/itemList");

const createList = asyncHandler ( async (req, res, next) => {
    const {name, description, groceries} = req.body;
    const data = await createListService(name, description, groceries);
    return res.status(201).json(data);
});

const getAllLists = asyncHandler ( async (req, res, next) => {
    const data = await getAllListsService();
    return res.json(data);
});

const getOneList = asyncHandler ( async (req, res, next) => {
    const data = await getOneListService(req.params.id);
    return res.json(data);
});

const deleteList = asyncHandler ( async (req, res, next) => {
    const data = await deleteListService(req.params.id);
    return res.json(data);
});

const updateList = asyncHandler ( async (req, res, next) => {
    const {id} = req.params;
    const {name, description, isCompleted} = req.body;
    const data = await updateListService(id, name, description, isCompleted);
    return res.json(data);
});

module.exports = {
    createList,
    getAllLists,
    getOneList,
    deleteList,
    updateList
};