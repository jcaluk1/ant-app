const asyncHandler = require("../middleware/asynchandler");
const { getAllGroceriesService } = require("../services/item");

const getAllGroceries = asyncHandler(async (req, res, next) => {
    const data = await getAllGroceriesService();
    return res.json(data);
});

module.exports = {
    getAllGroceries
};