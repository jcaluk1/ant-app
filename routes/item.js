const express = require("express");
const router = express.Router();

const { getAllGroceries } = require("../controles/item");

router.get("/", getAllGroceries);

module.exports = router;