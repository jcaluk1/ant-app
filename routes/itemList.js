const express = require("express");
const router = express.Router();

const { createList, getAllLists, getOneList, deleteList, updateList} = require("../controles/itemList");

const { validateGroceriesListCreate, validateId, validateGroceriesListPatch } = require("../middleware/validation/itemList");


router.get("/", getAllLists);
router.post("/", validateGroceriesListCreate, createList);
router.get("/:id", validateId, getOneList);
router.delete("/:id", validateId, deleteList);
router.patch("/:id", validateId, validateGroceriesListPatch, updateList);

module.exports = router;