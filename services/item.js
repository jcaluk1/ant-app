const { GroceryItem } = require("../models");

const getAllGroceriesService = async () => {
    const groceries = await GroceryItem.findAll();
    return { success: true, groceries };
};

module.exports = {
    getAllGroceriesService
};