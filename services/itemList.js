const { GroceryList, GroceryItem, ItemList } = require("../models");

const createListService = async (name, description, groceries) => {
    const list = await GroceryList.create({ name, description });
    const entries = groceries.map(gro => ({ grocerylistId: list.id, groceryitemId: gro.id, quantity: gro.quantity }));
    await ItemList.bulkCreate(entries);
    return { success: true, list };
};

const getAllListsService = async () => {
    const lists = await GroceryList.findAll({
        include: [
            {
                model: GroceryItem,
                attributes: ["id", "name", "image", "quantityUnit"],
                through: { attributes: ["quantity"] }
            }
        ]
    });
    return { success: true, lists };
};

const getOneListService = async (id) => {
    const list = await GroceryList.findOne({
        where: { id },
        include: [
            {
                model: GroceryItem,
                attributes: ["id", "name", "image", "quantityUnit"],
                through: { attributes: ["quantity"] }
            }
        ]
    });
    return { success: true, list };
};

const deleteListService = async (id) => {
    const numberOfDeleted = await GroceryList.destroy({ where: { id } });
    return { success: true, message: numberOfDeleted ? "List deleted" : "List not found" };
};

const updateListService = async (id, name, description, isCompleted) => {
    const numberOfUpdates = await GroceryList.update({ name, description, isCompleted }, { where: { id } });
    return { success: true, message: numberOfUpdates ? "List updated" : "List not found" };
};


module.exports = {
    createListService,
    getAllListsService,
    getOneListService,
    deleteListService,
    updateListService
};