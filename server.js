const express = require("express");
const app = express();
const cors = require("cors");
const path = require("path");

const corsOptions = {
  // origin: '*',
  methods: ["POST", "PUT", "GET", "OPTIONS", "DELETE", "HEAD", "PATCH"],
  credentials: true
};

app.use(cors(corsOptions));
app.use(express.static("./public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use("/api/list", require("./routes/itemList"));
app.use("/api/groceries", require("./routes/item"));

if (process.env.NODE_ENV === "production") {
  app.use(express.static("./client/build"));
  app.get("*", (req, res, next) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

process.on("uncaughtException", (err) => {
  console.log(`Uncaught exception: ${err.message}`);
  process.exit(1);
});

process.on("unhandledRejection", (reason, promise) => {
  console.log("Unhandled promise rejection at", promise, ` reason: ${reason}`);
  process.exit(1);
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`Server is running on port ${PORT}.`));