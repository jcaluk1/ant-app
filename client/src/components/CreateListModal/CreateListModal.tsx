import React, { useState } from "react";
import { Form, Input, List, Modal } from "antd";
import { CloseOutlined } from '@ant-design/icons';
import { GroceryItem } from "types/common";
import ListService from "services/ListService";
import GroceryItemForm from "./GroceryItemForm";

import "./CreateListModal.scss";

type CreateListModalProps = {
  visible?: boolean;
  onConfirm: () => void;
  onCancel: () => void;
}

const layout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 20 },
};

const CreateListModal: React.FC<CreateListModalProps> = ({ visible=false, onCancel, onConfirm }) => {
  const [chosenGroceries, setChosenGroceries] = useState<GroceryItem[]>([]);
  const [selectedGrocery, setSelectedGrocery] = useState<GroceryItem>({} as GroceryItem);
  const [createListForm] = Form.useForm();
  const [groceryItemForm] = Form.useForm();

  const addItem = ():void => {
    const isGroceryAlreadyAdded = chosenGroceries.find(grocery => grocery.id === selectedGrocery?.id);
    const isGroceryValid = selectedGrocery.id && selectedGrocery.quantity;
    if (!isGroceryValid) return;
    if(isGroceryAlreadyAdded) {
      setChosenGroceries(chosenGroceries.map(grocery => {
        if(grocery.id === selectedGrocery?.id) {
          return { ...selectedGrocery, quantity: +grocery.quantity + +selectedGrocery.quantity };
        }
        return grocery;
      }));
    } else {
      setChosenGroceries([...chosenGroceries, selectedGrocery]);
    }
    setSelectedGrocery({} as GroceryItem);
  };

  const removeItemFromList = (itemId: number) => {
    const filteredList = chosenGroceries.filter(item => item.id !== itemId);
    setChosenGroceries([...filteredList]);
  };
  
  const createList = ():void => {
    const list = {
      name: createListForm.getFieldValue('name'),
      description: createListForm.getFieldValue('description'),
      groceries: chosenGroceries.map(grocery => ({ id: grocery.id, quantity: grocery.quantity }))
    };
    ListService.createNewList(list).then(():void => {
      onConfirm();
      setChosenGroceries([]);
      createListForm.resetFields();
      groceryItemForm.resetFields();
    });
  };
  
  const cancel = ():void => {
    onCancel();
    setChosenGroceries([]);
    createListForm.resetFields();
    groceryItemForm.resetFields();
  };

  return <Modal title="Create new grocery list" visible={visible} onOk={createList} onCancel={cancel}>
    <Form
      {...layout}
      form={createListForm}
      name="newGroceryList"
      initialValues={{ remember: true }}
      onFinish={createList}
    >
      <Form.Item
        label="Name"
        name="name"
        rules={[{ required: true, message: 'Please input the list name' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Description"
        name="description"
        rules={[{ required: true, message: 'Please input description' }]}
      >
        <Input />
      </Form.Item>
    </Form>


    <Form.Item
      label="Grocery item"
      name="groceryItem"
      rules={[{ required: true, message: 'Please choose a grocery' }]}
    >
      <GroceryItemForm
        onSelection={(grocery: GroceryItem):void => setSelectedGrocery(grocery)}
        selectedGrocery={selectedGrocery}
        onSubmit={addItem}
        formRef={groceryItemForm}
      />
    </Form.Item>
      
    <List
      size="small"
      header={<h4>List preview</h4>}
      bordered
      dataSource={chosenGroceries}
      renderItem={item =>
        <List.Item>
          <CloseOutlined className="remove-icon" onClick={():void => removeItemFromList(item.id)}/>
          {item.quantity}{item.quantityUnit} {item.name}
        </List.Item>
      }
    />

  </Modal>;
};

export default CreateListModal;