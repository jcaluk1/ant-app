import React, { useEffect, useState } from "react";
import { Button, Form, FormInstance, Input, Select } from "antd";
import { Grocery, GroceryItem } from "types/common";
import GroceryService from "services/GroceryService";

import "./GroceryItemFrom.scss";

type GroceryItemFormProps = {
  onSelection: (grocery: GroceryItem) => void;
  selectedGrocery: GroceryItem;
  onSubmit: () => void;
  formRef: FormInstance;
}

const GroceryItemForm:React.FC<GroceryItemFormProps> = ({ onSelection, selectedGrocery, onSubmit, formRef }) => {
  const { Option } = Select;

  const [groceries, setGroceries] = useState<Grocery[]>([]);

  useEffect(() => {
    GroceryService.getGroceries().then((res) => {
      setGroceries(res.data.groceries);
    });
  }, []);

  const onAddItem = ():void => {
    onSubmit();
    formRef.resetFields();
  };
  
  return <Form
    form={formRef}
    layout="inline"
    id="grocery-item"
  >
    <Form.Item
      name="item"
      rules={[{ required: true, message: 'Please choose item' }]}
    >
      <Select onSelect={(value) => {
        onSelection({
          ...groceries.find(grocery => grocery.id === value) || {} as GroceryItem,
          quantity: 1
        });
        formRef.setFieldsValue({ quantity: 1 });
      }}>
        {
          groceries.map((grocery, index) =>
            <Option value={grocery.id} key={index}>
              <img alt={grocery.name} src={grocery.image} width="20"/>
              {grocery.name}
            </Option>
          )
        }
      </Select>
    </Form.Item>
    <Form.Item
      name="quantity"
      rules={[{ required: true, message: 'Please input item quantity' }]}
    >
      <Input
        type="number"
        name="quantity"
        suffix={selectedGrocery?.quantityUnit}
        min={0}
        max={99}
        onChange={(e) => onSelection({
          ...selectedGrocery,
          quantity: e.target.value as unknown as number
        })}
      />
    </Form.Item>
    <Form.Item>
      <Button type="primary" onClick={onAddItem}>Add item</Button>
    </Form.Item>
    
  </Form>;
};

export default GroceryItemForm;