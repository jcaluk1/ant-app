import React, { ReactNode } from "react";
import { Checkbox, Table } from "antd";
import { GroceryItem } from "types/common";

import "./GroceryTable.scss";

type GroceryTableProps = {
  data?: GroceryItem[];
  selectedRowKeys: number[];
  onSelect: (key: number) => void;
  isCompleted: boolean;
}

const GroceryTable: React.FC<GroceryTableProps> = ({ data, selectedRowKeys, onSelect, isCompleted }) => {
  const columns = [
    {
      title: "",
      key: "checkbox",
      render: (value: GroceryItem) => {
        return <Checkbox
          disabled={isCompleted || selectedRowKeys.includes(value.id)}
          defaultChecked={isCompleted}
          onClick={():void => onSelect(value.id)}
        />;
      },
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
      render: (text: string, record: any):ReactNode =>
        `${text} ${record.quantityUnit || ""}`,
    },
    {
      title: "Item",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "",
      dataIndex: "image",
      key: "image",
      width: 100,
      render: (src: string):ReactNode => <img src={src} alt={src} width="40px" />,
    },
  ];

  return (
    <Table
      className="grocery-table"
      columns={columns}
      dataSource={data}
    />
  );
};

export default GroceryTable;
