import React from "react";
import { useHistory } from "react-router-dom";
import { List, Card, Popconfirm, message } from "antd";
import { DeleteTwoTone, CheckCircleTwoTone } from '@ant-design/icons';
import ListService from "services/ListService";
import { GroceryList } from "types/common";

import "./GroceryLists.scss";

type GroceryListProps = {
  data: GroceryList[];
  refreshData: (newData:GroceryList[]) => void;
};

const GroceryLists: React.FC<GroceryListProps> = ({ data, refreshData }) => {
  const history = useHistory();

  const redirectToDetails = (id?: number): void => {
    if (id) history.push(`/${id}`);
  };

  const removeList = (id?: number, e?: React.MouseEvent<HTMLElement, MouseEvent>): void => {
    e?.stopPropagation();
    id && ListService.deleteListById(id).then(() => refreshData(data.filter(item => item.id !== id)));
    message.success('List deleted');
  };

  const cancel = (e?: React.MouseEvent<HTMLElement, MouseEvent>): void => {
    e?.stopPropagation();
    message.error('List removal canceled');
  };

  return (
    <List
      className="grocery-lists"
      dataSource={data}
      renderItem={(item) => (
        <Card
          title={item.name}
          extra={
            <>
              {item.isCompleted && <CheckCircleTwoTone twoToneColor="#52c41a" className="grocery-lists__icon"/>}
              <Popconfirm
                title="Are you sure to delete this list?"
                onConfirm={(e) => removeList(item.id, e)}
                onCancel={(e) => cancel(e)}
                okText="Yes"
                cancelText="No"
              >
                <DeleteTwoTone
                  twoToneColor="red"
                  className="grocery-lists__icon"
                  onClick={(e) => e.stopPropagation()}
                />
              </Popconfirm>
            </>
          }
          hoverable
          onClick={(): void => redirectToDetails(item.id)}
        >
          {item.description}
        </Card>
      )}
    />
  );
};

export default GroceryLists;
