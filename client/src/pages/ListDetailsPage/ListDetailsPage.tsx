import React, { useEffect, useState } from "react";
import { message, Progress } from "antd";
import { LeftOutlined } from '@ant-design/icons';
import { Link, useParams } from "react-router-dom";
import GroceryTable from "components/GroceryTable";
import ListService from "services/ListService";
import { HOME_PAGE_URL } from "routes/routeNames";
import { GroceryList } from "types/common";

import "./ListDetailsPage.scss";

type ListDetailsPagePathParams = {
  listId: string;
};

const ListDetailsPage: React.FC = () => {
  const { listId } = useParams<ListDetailsPagePathParams>();
  const [currentList, setCurrentList] = useState<GroceryList>({} as GroceryList);
  const [selectedRowKeys, setSelectedRowKeys] = useState<number[]>([]);
  const [percentage, setPercentage] = useState<number>(0);

  useEffect(() => {
    ListService.getListById(listId).then(res => {
      setCurrentList(res.data.list);
    });
  }, []);
  
  useEffect(()=> {
    if(currentList.groceryItems && selectedRowKeys.length === currentList.groceryItems.length) {
      ListService.editListStatus(listId, true).then(() => message.success("List is completed!")); 
    }
  }, [percentage]);

  const onSelectChange = (rowKey: number):void => {
    const newlySelectedKeys = [...selectedRowKeys, rowKey];
    setSelectedRowKeys(newlySelectedKeys);
    setPercentage(Math.ceil(newlySelectedKeys.length * 100 / currentList.groceryItems.length));
  };

  return (
    <div className="page">
      <Link className="back-link" to={HOME_PAGE_URL}><LeftOutlined />Back</Link>
      <h2>{currentList.name}</h2>
      <h4>{currentList.description}</h4>
      <Progress percent={currentList.isCompleted ? 100 : percentage} />
      <GroceryTable
        data={currentList?.groceryItems}
        selectedRowKeys={selectedRowKeys}
        onSelect={onSelectChange}
        isCompleted={currentList.isCompleted}
      />
    </div>
  );
};

export default ListDetailsPage;