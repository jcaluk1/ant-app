import React, { useEffect, useState } from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button } from "antd";
import GroceryLists from "components/GroceryLists";
import CreateListModal from "components/CreateListModal/CreateListModal";
import ListService from "services/ListService";
import { GroceryList } from "types/common";

import "./HomePage.scss";

const HomePage: React.FC = () => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>();
  const [lists, setLists] = useState<GroceryList[]>([]);

  useEffect(() => {
    ListService.getLists().then(res => {
      setLists(res.data.lists);
    });
  }, [isModalVisible]);

  const onConfirm = () => {
    setIsModalVisible(false);
  };

  const onCancel = () => {
    setIsModalVisible(false);
  };


  return (
    <div className="page">
      <div className="home-page__header">
        <h2>Your lists</h2>
        <Button
          type="primary"
          shape="circle"
          icon={<PlusOutlined />}
          size="large"
          onClick={() => setIsModalVisible(true)}
        />
      </div>
      <GroceryLists data={lists} refreshData={(newData: GroceryList[]) => setLists(newData)} />
      <CreateListModal visible={isModalVisible} onConfirm={onConfirm} onCancel={onCancel}/>
    </div>
  );
};

export default HomePage;
