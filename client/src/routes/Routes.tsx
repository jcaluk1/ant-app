import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "pages/HomePage";
import ListDetailsPage from "pages/ListDetailsPage";
import { LIST_DETAILS_PAGE_URL, HOME_PAGE_URL } from "./routeNames";

const Routes: React.FC = () => {
  const ScrollToTop = () => {
    window.scrollTo(0, 0);
    return null;
  };

  return (
    <Router>
      <Route path="/*" component={ScrollToTop} />
      <Switch>
        <Route path={HOME_PAGE_URL} component={HomePage} exact />
        <Route path={LIST_DETAILS_PAGE_URL} component={ListDetailsPage} exact />
      </Switch>
    </Router>
  );
};

export default Routes;
