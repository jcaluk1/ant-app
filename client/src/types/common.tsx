type DbInfo = {
  createdAt?: string;
  updatedAt?: string;
}

type GroceryUnit = "kg" | "l" | "dozen";

export type Grocery = {
  id: number;
  name: string;
  quantityUnit?: GroceryUnit;
  image?: string;
};

export type  GroceryItem = Grocery & { quantity: number };

export type GroceryList = DbInfo & {
  id?: number;
  name: string;
  description?: string;
  groceryItems: GroceryItem[];
  isCompleted: boolean;
};
