import { GroceryItem, GroceryList } from "types/common";

export const formatList = (list: any):GroceryList => ({
  id: list.id,
  name: list.name,
  description: list.description,
  isCompleted: list.isCompleted,
  groceryItems: list.groceryitems.map((item: any):GroceryItem => formatGroceryItem(item))
});

export const formatGroceryItem = (item:any):GroceryItem => ({
  id: item.id,
  image: item.image,
  name: item.name,
  quantityUnit: item.quantityUnit,
  quantity: item.itemlist.quantity
});