import axios from 'axios';
import { formatList } from "./ResponseFormatter";
import { GroceryList } from "../types/common";

const client = axios.create({
  baseURL: 'http://localhost:8080/api',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
});

class ListService {
  static async createNewList(list: any) {
    return await client.post("/list", list);
  }

  static async getLists() {
    const response = await client.get("/list");
    response.data.lists = response.data.lists.map((list: any): GroceryList => formatList(list));
    return response;
  }

  static async getListById(id: string) {
    const response = await client.get(`/list/${id}`);
    response.data.list = formatList(response.data.list);
    return response;
  }

  static async editListStatus(id: string, isCompleted: boolean) {
    return await client.patch(`/list/${id}`, { isCompleted });
  }

  static async deleteListById(id: number) {
    return await client.delete(`/list/${id}`);
  }
}

export default ListService;
