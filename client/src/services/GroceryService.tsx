import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:8080/api',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
});

class GroceryService {
  static async getGroceries() {
    return await client.get("/groceries");
  }
}

export default GroceryService;
