const errorHandler = (error, req, res, next) => {
    console.log({ error });
    res.status(400).json({ succes: false, error });
};

module.exports = {
    errorHandler
};