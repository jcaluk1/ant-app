const Joi = require("joi");
const asyncHandler = require("../asynchandler");

const groceriesListCreateSchema = Joi.object({
    name: Joi.string().required(),
    description: Joi.string(),
    groceries: Joi.array().items(Joi.object({
        id: Joi.number().integer().min(1).required(),
        quantity: Joi.number().required()
    })),
    isCompleted: Joi.bool()
});

const groceriesListPatchSchema = Joi.object({
    name: Joi.string(),
    description: Joi.string(),
    isCompleted: Joi.bool()
});

const validateGroceriesListCreate = asyncHandler ( async (req, res, next) => {
    await groceriesListCreateSchema.validateAsync(req.body);
    next();
});

const validateGroceriesListPatch = asyncHandler ( async (req, res, next) => {
    await groceriesListPatchSchema.validateAsync(req.body);
    next();
});


const validateId = asyncHandler ( async (req, res, next) => {
    await Joi.object({id: Joi.number().integer().min(1).required()}).validateAsync(req.params);
    next();
});

module.exports = {
    validateGroceriesListCreate,
    validateId,
    validateGroceriesListPatch
};